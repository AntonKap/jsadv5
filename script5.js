"use strict"

let urlUsers = "https://ajax.test-danit.com/api/json/users";
let urlPosts = "https://ajax.test-danit.com/api/json/posts";
let getContainer = document.querySelector(".conteiner");

class Card {
  constructor(name, email, id, title, body) {
    this.name = name;
    this.email = email;
    this.title = title;
    this.body = body;
    this.id = id;
    this.date = new Date();
    this.formattedDate = `${this.date.getDate()}.${this.date.getMonth() + 1}.${this.date.getFullYear()}`;
    this.render();
    this.addDeleteHandler();
  }

  render() {
    getContainer.insertAdjacentHTML(
      "beforeend",
      ` <div class="tweet-card" data-id="${this.id}">
          <div class="tweet-header">
            <img class="avatar" src="#" alt="Avatar">
            <div class="user-info">
              <h2 class="username">${this.name}</h2>
              <p class="email">${this.email}</p>
            </div>
          </div>
          <div class="tweet-content">
            <h2 class="tweet-text">${this.title}</h2>
            <p class="tweet-text">${this.body}</p>
          </div>
          <div class="tweet-footer">
            <span class="tweet-date">${this.formattedDate }</span>
            <div class="actions">
              <button class="btn-reply">Reply</button>
              <button class="btn-retweet">Retweet</button>
              <button class="btn-like">Like</button>
              <button class="btn-del">DELETE</button>
            </div>
          </div>
        </div> `
    );
  }

  addDeleteHandler() {
    const btnDel = document.querySelector(`.tweet-card:last-child .btn-del`);
    btnDel.addEventListener('click', () => {
      fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json'
        },
      })
      .then(({status}) => {
        if (status === 200) {
          const tweetCard = document.querySelector(`.tweet-card[data-id="${this.id}"]`);
          if (tweetCard) {
          tweetCard.remove();
          }
        }else{
          throw new Error('Network response was not ok');
        }
        
      })
      .catch(error => {
        console.error('Fetch error:', error);
       });
    });
  }



}

Promise.all([fetch(urlUsers), fetch(urlPosts)])
  .then((responses) => {
    return Promise.all(responses.map((response) => response.json()));
  })
  .then(([usersData, postsData]) => {
    usersData.forEach((user) => {
      postsData.forEach((post) => {
        if (user.id === post.userId) {
          let card = new Card(user.name, user.email, post.id, post.title, post.body);
        }
      });
    });
  })
  .catch((error) => {
    console.error("Error fetching data:", error);
  });

